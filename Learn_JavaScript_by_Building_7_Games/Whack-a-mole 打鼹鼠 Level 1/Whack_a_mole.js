var imgs; //全局的
//遊戲時間20 秒
var gameTime = 0;
//定時器
var chImg; // 代表 window.setInterval("changeImg()",1000)
var cutTime; // 代表 window.setInterval("counterTime()",1000)
var mouseBk; // 代表 window.setTimeout("mouseBack(" + index + ")",1000)

//打中的個數
var count = 0;

//開始遊戲的方法
function start() {
    gameTime = 20;
    count = 0;
    //要求1秒鐘調用一次,使用定時器功能
    chImg = window.setInterval("changeImg()",1000);
    cutTime = window.setInterval("counterTime()",1000);
}

//每隔一秒鐘,隨機的切換表格中的某一個格子的背景圖片
//切換成地鼠鑽出來的圖片
function changeImg(){
    //獲得所有的img 對象, 返回的是一個數組
    imgs = document.getElementsByTagName("img");
    //alert(imgs.length); //輸出的是25
    //獲得一個隨機的群組的索引下標,向下取整
    var index = Math.floor(Math.random()*imgs.length); // Math.random()*imgs.length 的取值長度是0-24.99999, 而 Math.random() 的取值是0-1
    // Math.floor 表示向下取整,就是可以取得0-24的值
    // 獲得隨機的一個圖片對象
    var img = imgs[index];
    //切換img 的圖片資源
    img.src="01.jpg";

    //一秒鐘之後, 被切換的資源還原
    mouseBk = window.setTimeout("mouseBack(" + index + ")",1000); // 表示1秒鐘之後,執行 mouseBack, 將index 的位置還原成img.src = "mole.jpg";
    //因為上面是把index傳進來,所以要用變量 " + index + " 把index 傳進來, 直接寫成(index)是錯誤的,因為它是一個字符串string
}

//讓隨機出現的地鼠資源還原為初始狀態, index 是圖片數組的索引
function mouseBack(index){
    var img = imgs[index];
    img.src = "00.jpg";

}

//當使用鼠標點擊25個圖片資源的時候, 將圖片資源切換為被打的老鼠的資源
function hit(img){
    //如果當前的img 對象是的時候, 才進行切換
    var name = img.src;
    //求字符串的子串對象, 只獲得最後的6 個字符的內容
    var subName = name.substr(name.length-6);
    //alert(name);
    //當被點擊的圖片的資源是mouse.jpg 的時候, 進行資源的切換
    if (subName == "01.jpg"){
        img.src = "02.jpg";
        count ++; //代表被擊中
    }
}

//計時的方法, 該方法要求遊戲啟動後, 每隔一秒鐘執行一次
function counterTime(){
    gameTime--; // -- 代表自減的意思
    //更新遊戲的剩餘的時間
    var game = document.getElementById("gametime");
    game.innerHTML = gameTime;
    if(gameTime == 0){
        //遊戲結束
        gameOver();
    }
}

//遊戲結束, 要清理資源 例如: 定時器 setTimeout
function gameOver(){
    //停掉計時器
    window.clearInterval(chImg);
    window.clearInterval(cutTime);
    window.clearInterval(mouseBk);
    //將表格中的所有的圖片資源重置
    for(var i in imgs){
        imgs[i].src = "00.jpg";
    }
    alert("遊戲結束,count = " + count);
}