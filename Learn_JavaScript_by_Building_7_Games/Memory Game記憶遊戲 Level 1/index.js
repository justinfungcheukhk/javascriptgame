/* 要在单击时翻转卡片，我们需要向元素添加类别flip（意为翻转）。 
为此，让我们使用document.querySelectorAll选择所有的memory-card元素。 
然后使用forEach循环遍历它们并附加一个事件监听器。 
每次卡片被点击时，都会触发flipCard（意为翻转卡片）功能。 
this变量表示被单击的卡片。 该函数访问元素的classList并切换flip类：
 */
const cards = document.querySelectorAll('.memory-card');
let hasFlippedCard = false;
let lockBoard = false;
let firstCard, secondCard;
/* 在我們已經完成翻轉卡片的功能之後，接下來我們來處理匹配的邏輯。
當我們點擊第壹張牌時，它需要等待另壹張牌被翻轉。變量hasFlippedCard和flippedCard將管理翻轉狀態。
如果沒有翻轉的卡片，hasFlippedCard被設置為true, flippedCard被設置為已點擊的卡片。讓我們切換toggle方法來add（意為添加）：
 */
function flipCard(){
    if (lockBoard) return;
    if (this === firstCard) return; /* 可能会出现玩家在同一张卡上点击两次的情况。 如果匹配条件判断为true，从该卡片上删除事件侦听器。
    为了防止这种情况，需要检查当前点击的卡片是否等于firstCard，如果是肯定的则返回。*/


    this.classList.add('flip');

/* 所以現在我們可以通過訪問兩個卡片數據集來檢查匹配。 
讓我們將匹配邏輯提取到它自己的方法checkForMatch()，並將hasFlippedCard設置為false。 
如果匹配，則調用disableCards()並分離兩個卡片上的事件偵聽器，以防止再壹次翻轉。 
否則，unflipCards()會將兩張卡都恢復成超過1500毫秒的超時，從而刪除.flip類：把全部代碼組合在壹起：*/
    if (!hasFlippedCard) {
        hasFlippedCard = true;
        firstCard= this;
        return;    
    }

    secondCard = this;
    hasFlippedCard = false;

    checkForMatch();
}

function checkForMatch() {
    let isMatch = firstCard.dataset.name === secondCard.dataset.name;
    isMatch ? disableCards() : unflipCards();
}

function disableCards() {
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);

    resetBoard();
}

/* 現在我們已經完成了匹配邏輯，接著為了避免同時轉動兩組卡片，我們還需要鎖定它們，否則翻轉將會失敗。
我們先聲明壹個lockBoard變量。 當玩家點擊第二張卡片時，lockBoard將設置為true，
條件 if (lockBoard) return;在卡片被隱藏或匹配之前會阻止其他卡片翻轉： */
function unflipCards() {
    lockBoard =true;

    setTimeout(() => {
        firstCard.classList.remove('flip');
        secondCard.classList.remove('flip');

        lockBoard = false;
        resetBoard();
    }, 1500);
}

/* 变量 firstCard和 secondCard需要在每一轮之后被重置，所以让我们将它提取到一个新方法 resetBoard()中， 
再其中写上 hasFlippedCard = false;和 lockBoard = false。ES6的解构赋值功能 [var1, var2] = ['value1', 'value2']允许我们把代码写得超短：*/
function resetBoard() {
    [hasFlippedCard, lockBoard] = [false, false];
    [firstCard, secondCard] = [null, null];
}

/* 我們的遊戲現在看起來相當不錯，但是如果不能洗牌就失去了樂趣，所以我們現在來處理這個功能。
當 display: flex在容器上被聲明時，flex-items會按照組和源的順序進行排序。 每個組由order屬性定義，該屬性包含正整數或負整數。 
默認情況下，每個flex-item都將其order屬性設置為0，這意味著它們都屬於同壹個組，並將按源的順序排列。 如果有多個組，則首先按組升序順序排列。
遊戲中有12張牌，因此我們將叠代它們，生成0到12之間的隨機數並將其分配給flex-item order屬性： 
为了调用shuffle函数，让它成为一个立即调用函数表达式（IIFE），这意味着它将在声明后立即执行。 脚本应如下所示：*/
(function shuffle() {
    cards.forEach(card => {
        let ramdomPos = Math.floor(Math.random() * 12);
        cards.style.order = ramdomPos;
    });
})();

cards.forEach(card => card.addEventListener('click',flipCard));

/* 編寫匹配條件的更簡練的方法是使用三元運算符。 它由三個塊組成。 
第壹個塊是要判斷的條件。 如果條件符合就執行第二個塊，否則執行的塊是第三個： */
/* if (firstCard.dataset.name === secondCard.dataset.name) {
    disableCards();
    return;
}

unflipCards();

let isMatch = firstCard.dataset.name === secondCard.dataset.name;
isMatch ? disableCards() : unflipCards();